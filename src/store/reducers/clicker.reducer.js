import {
  ACTION_ADD_COINS,
  ACTION_SUBTRACT_COINS,
  ACTION_ADD_DIAMONDS,
} from '../../constants';

const INITIAL_STATE = {
  coins: 0,
  diamonds: 0,
  medals: [],
};

const clicker = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case ACTION_ADD_COINS:
      return {
        ...state,
        coins: state.coins + action.payload,
      };
    case ACTION_ADD_COINS:
      return {
        ...state,
        coins: state.coins + action.payload,
      };
    case ACTION_SUBTRACT_COINS:
      return {
        ...state,
        coins: state.coins - action.payload,
      };
    default:
      return state;
  }
};
export default clicker;
