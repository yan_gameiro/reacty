import { CORRECT_ACCCESS_TOKEN, LOGIN } from '../../constants';

const INITIAL_STATE = {
  correctAccessToken: CORRECT_ACCCESS_TOKEN,
  isUserAuthenticated: false,
};

const login = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case LOGIN:
      return {
        ...state,
        isUserAuthenticated: action.payload,
      };
    default:
      return state;
  }
};

export default login;
