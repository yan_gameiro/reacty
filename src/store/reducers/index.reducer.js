import { combineReducers } from 'redux';

import login from './login.reducer';
import clicker from './clicker.reducer';

export default combineReducers({ login, clicker });
