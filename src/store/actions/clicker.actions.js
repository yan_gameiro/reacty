import {
  ACTION_ADD_COINS,
  ACTION_SUBTRACT_COINS,
  ACTION_ADD_DIAMONDS,
} from '../../constants';

export const addCoins = payload => {
  return {
    type: ACTION_ADD_COINS,
    payload,
  };
};

export const addDiamonds = payload => {
  return {
    type: ACTION_ADD_DIAMONDS,
    payload,
  };
};

export const subtractCoins = payload => {
  return {
    type: ACTION_SUBTRACT_COINS,
    payload,
  };
};
