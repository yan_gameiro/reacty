import React from 'react';

import { BrowserRouter, Route, Switch } from 'react-router-dom';
import { connect } from 'react-redux';

import Home from './pages/home';
import Login from './pages/login';
import Clicker from './pages/clicker';
import RestrictedArea from './pages/RestrictedArea';
import PageNotFound from './pages/PageNotFound';
import Annotations from './pages/Annotations';

const Routes = props => {
  if (props.isUserAuthenticated) {
    return (
      <BrowserRouter>
        <Switch>
          <Route exact path="/" component={Home} />
          <Route exact path="/clicker" component={Clicker} />
          <Route exact path="/restricted" component={RestrictedArea} />
          <Route exact path="/annotations" component={Annotations} />
          <Route component={PageNotFound} />
        </Switch>
      </BrowserRouter>
    );
  }
  if (!props.isUserAuthenticated) {
    return (
      <BrowserRouter>
        <Switch>
          <Route exact path="/" component={Home} />
          <Route exact path="/login" component={Login} />
          <Route exact path="/clicker" component={Clicker} />
          <Route component={PageNotFound} />
        </Switch>
      </BrowserRouter>
    );
  }
};

const mapStateToProps = props => ({
  isUserAuthenticated: props.login.isUserAuthenticated,
});

export default connect(mapStateToProps)(Routes);
