import React from 'react';

const PageNotFound = ({ location }) => {
  return (
    <h1>
      The page "{location.pathname.substring(1)}" doesn't exists, or you don't
      have the privileges :'({' '}
    </h1>
  );
};

export default PageNotFound;
