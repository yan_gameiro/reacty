import React, { Component } from 'react';
import { connect } from 'react-redux';
import { login } from '../store/actions/login.actions';
import PropTypes from 'prop-types';

import LoadingAnimation from '../components/loadingAnimation';

class Login extends Component {
  state = {
    isLoading: true,
    insertedToken: '',
  };
  static contextTypes = {
    router: PropTypes.object,
  };

  endLoading = () => {
    setTimeout(() => {
      this.setState({ isLoading: false });
    }, 5000);
  };

  verifyInsertedToken = () => {
    var correctToken =
      this.props.correctAccessToken === this.state.insertedToken;
    if (correctToken) {
      this.props.login(true);
      this.props.history.push('restricted');
    }
  };

  componentDidMount() {
    this.endLoading();
  }

  render() {
    const { isLoading } = this.state;
    const { isUserAuthenticated } = this.props;

    if (isLoading) {
      return <LoadingAnimation />;
    }

    if (isUserAuthenticated === true) {
      return <p> Successful Login! </p>;
    }

    return (
      <div>
        <p>Access Token:</p>

        <input
          onChange={event => {
            if (event.target.value !== '') {
              this.setState({ insertedToken: event.target.value });
            }
          }}
        />

        <button onClick={this.verifyInsertedToken}>Go</button>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    correctAccessToken: state.login.correctAccessToken,
    isUserAuthenticated: state.login.isUserAuthenticated,
  };
};

const mapDispatchToProps = dispatch => {
  return { login: isUserAuthenticated => dispatch(login(isUserAuthenticated)) };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Login);
