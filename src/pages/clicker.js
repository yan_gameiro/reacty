import React from 'react';
import './clicker.css';

import Scoreboard from '../components/Scoreboard';
import FreneticClickers from '../components/FreneticClickers';

const Clicker = () => {
  return (
    <div className="page-body">
      <Scoreboard />
      <FreneticClickers />
    </div>
  );
};

export default Clicker;
