import React from 'react';
import './home.css';

const Home = () => {
  return (
    <div className="centered-body">
      container with display: block;
      <div className="container-1">
        <div className="row-1"> Notebook Gamer </div>
        <div className="row-1"> OK </div>
        <div className="row-1"> Bye Bye </div>
        <div className="row-1"> NYC </div>
      </div>
      <div className="white-space"></div>
      container with display: flex;
      <div className="container-2">
        <div className="row-1"> Notebook Gamer </div>
        <div className="row-1"> OK </div>
        <div className="row-1"> Bye Bye </div>
        <div className="row-1"> NYC </div>
      </div>
      <div className="white-space"></div>
      container with display: flex; flex-wrap: wrap;
      <div className="container-3">
        <div className="row-1">Maria Luciana Silva </div>
        <div className="row-1"> Yan Vinicius </div>
        <div className="row-1"> Aurora Allana Carvalho </div>
        <div className="row-1"> Bianca Jaqueline Sophie das Neves </div>
        <div className="row-1"> Ronald Rump </div>
        <div className="row-1"> Otávio Matheus Nascimento </div>
      </div>
      <div className="white-space"></div>
      items with flex: 1; flex: 1; flex: 2;
      <div className="container-3">
        <div className="row-2"> 1 </div>
        <div className="row-2"> 2 </div>
        <div className="row-3"> 3 </div>
      </div>
      <div className="white-space"></div>
      flex-direction: column;
      <div className="container-4">
        <div className="row-2"> 1 </div>
        <div className="row-2"> 2 </div>
        <div className="row-3"> 3 </div>
      </div>
      <div className="white-space"></div>
      flex-direction: row;
      <div className="container-5">
        <div className="row-2"> 1 </div>
        <div className="row-2"> 2 </div>
        <div className="row-3"> 3 </div>
      </div>
    </div>
  );
};

export default Home;
