import React from 'react';

const RestrictedArea = () => {
  return (
    <div>
      <h1>Hello, friend</h1>
      <p>If you see this, you're logged in!</p>
      <a href="/annotations"> -> go to Annotations</a>
    </div>
  );
};

export default RestrictedArea;
