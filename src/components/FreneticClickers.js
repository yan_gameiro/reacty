import React from 'react';
import { connect } from 'react-redux';
import { addCoins, subtractCoins } from '../store/actions/clicker.actions';
import './freneticClickers.css';
import FreneticButton from './FreneticButton';

const FreneticClickers = props => {
  const reset = () => props.subtractCoins(props.coins);
  const earnCoins = ammount => props.addCoins(ammount);
  const handleFreneticClick = number => {
    earnDiamonds(number);
    earnCoins(number);
  };

  const earnDiamonds = number => {
    const randomNumber = Math.floor(Math.random() * 100 + 1);
    if (randomNumber <= 10) {
    }
  };

  return (
    <div className="frenetic-clickers noselect">
      <button className="reset-button" onClick={reset}>
        RESET
      </button>
      <FreneticButton
        action={() => handleFreneticClick(1)}
        insideText="+ 1 coin"
      />
      <FreneticButton
        action={() => handleFreneticClick(5)}
        insideText="+ 5 coins"
      />
      <FreneticButton
        action={() => handleFreneticClick(25)}
        insideText="+ 25 coins"
      />
      <FreneticButton
        action={() => handleFreneticClick(60)}
        insideText="+ 60 coins"
      />
    </div>
  );
};

const mapStateToProos = state => {
  return {
    coins: state.clicker.coins,
  };
};

function mapDispatchToProps(dispatch) {
  return {
    addCoins: number => dispatch(addCoins(number)),
    subtractCoins: number => dispatch(subtractCoins(number)),
  };
}

export default connect(
  mapStateToProos,
  mapDispatchToProps,
)(FreneticClickers);
