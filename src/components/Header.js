import React from 'react';
import './Header.css';
import Logout from '../components/Logout';
import { connect } from 'react-redux';

const Header = props => {
  return (
    <header id="main-header">
      <div className="headerItem">
        <a href="/">
          <p> Home</p>
        </a>
      </div>
      <div className="headerItem">
        <a href="/clicker"> Clicker</a>
      </div>
      <div className="headerItem">
        <a href="/restricted"> Restricted Area</a>
      </div>
      <div className="login-logout">
        {props.isUserAuthenticated ? (
          <Logout />
        ) : (
          <div className="headerItem">
            <a href="/login"> Login</a>
          </div>
        )}
      </div>
    </header>
  );
};

const mapStateToProps = state => {
  return {
    isUserAuthenticated: state.login.isUserAuthenticated,
  };
};

export default connect(mapStateToProps)(Header);
