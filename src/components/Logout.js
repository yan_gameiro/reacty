import React from 'react';
import { connect } from 'react-redux';
import { login } from '../store/actions/login.actions';

import './Logout.css';

const Logout = props => {
  const logout = () => {
    props.login(false);
  };
  return (
    <div className="headerItem">
      <p className="pointer" onClick={logout}>
        Logout
      </p>
    </div>
  );
};

const mapDispatchToProps = dispatch => {
  return { login: isUserAuthenticated => dispatch(login(isUserAuthenticated)) };
};

export default connect(
  null,
  mapDispatchToProps,
)(Logout);
