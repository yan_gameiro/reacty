import React from 'react';
import { connect } from 'react-redux';
import './scoreboard.css';

const Scoreboard = ({ coins }) => (
  <div className="scoreboard">
    <div className="scoreboard-item">
      <p>
        <span className="big-number-1">{coins}</span> Coins
      </p>
    </div>
    <div className="scoreboard-item">
      <p>
        <span className="big-number-1">0 </span> Diamonds
      </p>
    </div>
    <div className="scoreboard-item">
      <p>
        <span className="big-number-1">0 </span> Medals
      </p>
    </div>
  </div>
);

const mapStateToProps = state => {
  return {
    coins: state.clicker.coins,
  };
};

export default connect(mapStateToProps /*, mapDispatchToProps*/)(Scoreboard);
