import React from 'react';
import arrowGray from '../img/arrow-gray.png';
import arrowGreen from '../img/arrow-green.png';

const FreneticButton = ({ action, insideText }) => {
  return (
    <div>
      <div className="clicker-button" onClick={action}>
        <div className="txt-inside-button">{insideText}</div>
      </div>
      <div className="clicker-label">
        <div className="dots">
          <img alt="arrow upgrade" className="img-dots" src={arrowGreen}></img>
          <img
            alt="arrow upgrade"
            className="img-dots not-buyed-arrow"
            src={arrowGray}></img>
          <img
            alt="arrow upgrade"
            className="img-dots not-buyed-arrow"
            src={arrowGray}></img>
          <img
            alt="arrow upgrade"
            className="img-dots not-buyed-arrow"
            src={arrowGray}></img>
          <img
            alt="arrow upgrade"
            className="img-dots not-buyed-arrow"
            src={arrowGray}></img>
        </div>
        <div className="clicker-upgrade">UPGRADE</div>
      </div>
    </div>
  );
};

export default FreneticButton;
