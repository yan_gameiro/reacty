import React, { Component } from 'react';

class LoadingAnimation extends Component {
  state = {
    dots: '[ ][ ][ ]',
    stage: 0,
  };

  changeDots = () => {
    const { stage } = this.state;
    setTimeout(() => {
      if (stage === 0) {
        this.setState({ dots: '[x][ ][ ]', stage: stage + 1 });
      }

      if (stage === 1) {
        this.setState({ dots: '[ ][x][ ]', stage: stage + 1 });
      }

      if (stage === 2) {
        this.setState({ dots: '[ ][ ][x]', stage: 0 });
      }
    }, 300);
  };

  render() {
    this.changeDots();
    const { dots } = this.state;
    return <p>{dots}</p>;
  }
}

export default LoadingAnimation;
