import React, { Component } from 'react';
import './card.css';
import imgRed from '../img/full-red.png';
import imgBlue from '../img/full-blue.png';

export default class Card extends Component {
  state = {
    typedText: '',
    isEditing: false,
  };

  onHover = () => 'blabla';

  render() {
    let { typedText, isEditing } = this.state;
    const { title, body, id } = this.props;
    return (
      <div className="card">
        <div className="cardHeader">
          <div className="cardTitle">{title}</div>
          <div className="cardButtons">
            <div
              onClick={() => {
                if (isEditing) {
                  this.setState({ isEditing: false });
                  this.props.editAction(id, typedText);
                } else {
                  this.setState({ isEditing: true });
                }
              }}>
              <img alt="edit button" src={imgBlue}></img>
            </div>
            <div
              onClick={() => {
                this.props.deleteAction(id);
              }}>
              <img alt="delete button" src={imgRed}></img>
            </div>
          </div>
        </div>
        <hr></hr>
        {isEditing ? (
          <div className="cardBody">
            <input
              onBlur={typedText => {
                this.setState({ typedText: typedText.target.value });
              }}></input>
          </div>
        ) : (
          <div className="cardBody">{body}</div>
        )}
      </div>
    );
  }
}
