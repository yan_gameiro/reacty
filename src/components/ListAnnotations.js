import React, { Component } from 'react';
import Card from './Card';
import api from '../services/api';
import './listAnnotations.css';
import imgGreen from '../img/full-green.png';

export default class ListAnnotations extends Component {
  state = {
    annotations: [],
    typedText: '',
  };

  createAnnotation = async text => {
    return api.post('/annotations', { text });
  };

  loadAnnotations = async () => {
    const response = await api.get(`/annotations`);
    const { annotations } = response.data;

    this.setState({ annotations: annotations });
  };

  updateAnnotation = async (annotationId, text) => {
    await api.post(`/annotations/${annotationId}`, {
      text,
    });
  };

  deleteAnnotation = async annotationId => {
    await api.delete(`/annotations/delete/${annotationId}`);
  };

  async componentDidMount() {
    await this.loadAnnotations();
  }

  editAction = (id, typedText) => {
    this.updateAnnotation(id, typedText);
    let filtered = this.state.annotations.filter(
      annotation => annotation.id !== id,
    );
    let newArr = [...filtered, { id: id, text: typedText }];

    this.setState({
      annotations: newArr.sort((a, b) => {
        return a.id - b.id;
      }),
    });
  };

  deleteAction = id => {
    this.deleteAnnotation(id);
    let filtered = this.state.annotations.filter(
      annotation => annotation.id !== id,
    );
    this.setState({
      annotations: filtered,
    });
  };

  createAction = async () => {
    const result = await this.createAnnotation(this.state.typedText);
    this.setState({
      annotations: [
        ...this.state.annotations,
        { id: result.data.newId, text: this.state.typedText },
      ],
    });
    this.setState({ typedText: '' });
  };

  render() {
    return (
      <div>
        <h1>Annotations:</h1>
        <div className="newAnnotation">
          <input
            value={this.state.typedText}
            className="input-new-annotation"
            onChange={typedText => {
              this.setState({ typedText: typedText.target.value });
            }}></input>
          <div
            className="button-new-annotation"
            onClick={() => {
              this.createAction();
            }}>
            <img
              className="create-button"
              alt="create button"
              src={imgGreen}></img>
          </div>
        </div>
        <div className="annotations-list">
          {this.state.annotations.map((annotation, i) => {
            return (
              <Card
                title={'annotation ' + annotation.id}
                body={annotation.text}
                id={annotation.id}
                isEditing={annotation.id === this.state.idBeingEditing}
                editAction={this.editAction}
                deleteAction={this.deleteAction}
                key={annotation.id}
              />
            );
          })}
        </div>
      </div>
    );
  }
}
