import React from 'react';
import './app.css';
import { Provider } from 'react-redux';
import Routes from './routes';
import Header from './components/Header';

import store from './store/store';

const App = () => (
  <Provider store={store}>
    <div className="App">
      <Header />
      <Routes />
    </div>
  </Provider>
);

export default App;
