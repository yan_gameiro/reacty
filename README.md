This is a simple test project that I made when I was starting to study React

The reference branch is named `master`, but you can give a look on branch `T02` as well

# How to run

1 - Clone the project
`git clone https://yan_gameiro@bitbucket.org/yan_gameiro/reacty.git`

2 - install dependencies
`yarn install`

3 - start
`yarn start`

# Explanations

## Home

Is an simple example about flex containers  
![Print of home session](readme-img/reacty-home.png)


## Clicker

Is an incomplete clicker game, where you can gain coins in different ways and use these coins to buy upgrades 
![Print of clicker session](readme-img/reacty-clicker.png)

## Login

Is an simple and insecure way of implementing login with token. One token that you can use is abc123 and it is defined on `src/constants.js`